# Realizar un código que lea números primos y los no
# primos.

# Autor: Brayan Gonzalo Cabrera Cabrera.

# brayan.cabrera@unl.edu.ec

def numero_primo ():
    while True:
        try:
            num = input("Ingrese un Numero (digite fin para terminar proceso")
            cont = 0
            if num.lower() in "fin":
                print()
                print("Programa Terminado")
                break
            num = int(num)
            for i in range(1, num + 1):
                if num % i == 0:
                    cont += 1
            if cont == 2:
                    print("El numero", [num], "es Primo.")
            else:
                    print("El numero ", [num], " No es Primo.")

        except ValueError:
                    print("Ingrese un numero Valido!")

numero_primo()
